﻿var RoombaController = function(container, url) {
  let self = this;
  const ws1 = 'ws://';
  const ws2 = '/websocket/controller';
  const wsUrl = ws1 + url + ws2;

  let webSckt = null;
  let state_columns = {};
  let state_table = null;

  if (container) {
      // ラッパーDIV
      state_table = document.createElement('table');

      for (let c of ["接続状態", "操作端末", "ルンバ電池残量", "PC電池残量"]) {
        let tr = document.createElement('tr');
        let th = document.createElement('th');
        let td = document.createElement('td');
        tr.appendChild(th);
        tr.innerText = c + ":";
        tr.appendChild(td);
        state_columns[c] = td;
        state_table.appendChild(tr);
      }
    
      container.appendChild(state_table);
  }

  let showState = function(name, val) {
    if (state_columns[name]) {
      state_columns[name].innerText = val;
    }
  };

  let connectWebSocket = function() {
    try {
      showState("接続状態", "接続中");
      //console.log(wsUrl)
      webSckt = new WebSocket(wsUrl);
    } catch (e) {
      callErrorHandler('WebSocket接続失敗(' + wsUrl + ')');
    }

    webSckt.onopen = function(evt) {
      showState("接続状態", "正常");
      console.log("Rommba : open");
      initController();
    };
    webSckt.onerror = function(err) {
      showState("接続状態", "異常");
      showState("ルンバ電池残量", "");
      showState("PC電池残量", "");
      webSckt.close();
    };
    webSckt.onmessage = function(evt) {
      let roombaMode;
      let roombaLevel;
      let roombaCurrent;
      let pcLevel;
      let message = JSON.parse(evt.data);
      console.log(message)
      for(key in message){
        if(key == "MODE"){
          roombaMode = message[key];
        }
        else if(key == "LEVEL"){
          roombaLevel = message[key];
        }
        else if(key == "CURRENT"){
          roombaCurrent = message[key];
        }
        else if(key == "PC BATTERY"){
          pcLevel = message[key];
        }
      }
      if(roombaMode== "Full"){
        showState("接続状態", "操作可能");
      }else{
        showState("接続状態", "正常");
      }
      if(roombaCurrent == "Charging"){
        showState("ルンバ電池残量", roombaLevel + "　充電中");
      }else{
        showState("ルンバ電池残量", roombaLevel);
      }
      showState("PC電池残量", pcLevel);
    };
    webSckt.onclose = function(evt) {
      showState("接続状態", "異常");
      showState("ルンバ電池残量", "");
      showState("PC電池残量", "");
      terminateController();
      //webSckt = null;
      retryWebSocket();
    };
  };

  let isRetry = false;
  let retryWebSocket = function() {
    if (isRetry) {
      setTimeout(connectWebSocket, 3000);
    }
    
  };

  ///////////////////////////////////////////////////////
  // コントローラの処理
  ///////////////////////////////////////////////////////

  let gamepad = null;
  let axes = null;
  let buttonList = [];
  let intervalID = null;
  let gamepad_id = "";
  let ButtonManager = function(number, key, cmd_down, cmd_up) {
    this.number   = number;
    this.key      = key;
    this.cmd_down = cmd_down;
    this.cmd_up   = cmd_up;
    this.pressed  = false;
    this.state    = false;
  };

  //
  let processKeyEvent = function(key, isDown) {
    if (!gamepad) {
      showState("操作端末", "キーボード");
      switch(key) {
        case 'ArrowUp':
          axes[1] = isDown ? +1.0 : 0.0;
          break;
        case 'ArrowDown':
          axes[1] = isDown ? -1.0 : 0.0;
          break;
        case 'ArrowLeft':
          axes[0] = isDown ? -1.0 : 0.0;
          break;
        case 'ArrowRight':
          axes[0] = isDown ? +1.0 : 0.0;
          break;
      }
    }
    for (let btn of buttonList) {
      if (btn.key == key.toLowerCase()) {
        btn.pressed = isDown;
      }
    }
  };

  let sedControllerEvent = function() {
    gamepad = null;
    var gamepadList = navigator.getGamepads();
    for (var i = 0; i < gamepadList.length; i++) {
      if (gamepadList[i] && gamepadList[i].id.indexOf("x") != -1 ) {
        gamepad = gamepadList[i];
        break;
      }
    }
    if (gamepad) {
      showState("操作端末", "ゲームパッド");
      axes[0] = +gamepad.axes[0];
      axes[1] = -gamepad.axes[1];
      if (-0.1 < axes[0] && axes[0] < +0.1) {
        axes[0] = 0.0;
      }
      if (-0.1 < axes[1] && axes[1] < +0.1) {
        axes[1] = 0.0;
      }
      for (let btn of buttonList) {
        btn.pressed = gamepad.buttons[btn.number].pressed;
      }
    }

    if (webSckt != null && webSckt.readyState == WebSocket.OPEN) {
      let obj;
      // AxesEvent
      obj = { type: 'gamepad_axes',
              x   : axes[0].toFixed(2),
              y   : (axes[1] * 1.0).toFixed(2)
            };
      webSckt.send(JSON.stringify(obj));

      // ButtonEvent
      for (let btn of buttonList) {
        if(btn.state != btn.pressed){
          let command = btn.pressed ? btn.cmd_down : btn.cmd_up;
          if (command) {
            let obj = { type   : 'gamepad_command',
                        command: command
                      };
            webSckt.send(JSON.stringify(obj));
          }
          btn.state = btn.pressed;
        }
      }
    }
  };

  let initController = function() {
    axes = [0, 0];
    for (let btn of buttonList) {
      btn.pressed  = false;
      btn.state    = false;
    }
    window.onkeydown = function(event) {
      processKeyEvent(event.key, true);
    }
    window.onkeyup = function(event) {
      processKeyEvent(event.key, false);
    }
    intervalID1 = window.setInterval(sedControllerEvent, 30);
  };

  let terminateController = function() {
    clearInterval(intervalID);
    window.onkeydown = null;
    window.onkeyup = null;
    gamepad = null;
  };

  //////////////////////////////////////////////////
  ////
  //////////////////////////////////////////////////

  window.addEventListener("gamepadconnected", function(e) {
    showState("操作端末", "ゲームパッド");
  });

  window.addEventListener("gamepaddisconnected", function(e) {
    showState("操作端末", "キーボード");
  });

  self.start = function() {
    isRetry = true;    
    

    // var productId = "046D";
    // var venderId = "C21D";
    // var serialNumber = "E911386E";
       

    connectWebSocket();
  };

    //   navigator.usb.requestDevice({ 'filters': [
  //     { 'vendorId': venderId, 'productId': productId }
  //   ]
  //   }).then(device => {
    
  //     console.log('success!');
  //     console.log(device.name);
  //     //login();
  //     //return device.open;
    
  // }).catch(error => {
  //   console.log(error);
  // });
  // };

  self.stop = function() {
    isRetry = false;
    webSckt.close();
  };

  self.addButtonCommand = function(number, key, cmd_down, cmd_up) {
    buttonList.push(new ButtonManager(number, key, cmd_down, cmd_up));
  }

  self.roombaDockin = function() {
    let obj = { type   : 'gamepad_command',
                command: "comman_di_up"
              };
    webSckt.send(JSON.stringify(obj));
  }

  self.roombaReset = function() {
    let obj = { type   : 'gamepad_command',
                command: "comman_r_up"
              };
    webSckt.send(JSON.stringify(obj));
  }

};
