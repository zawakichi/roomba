﻿var RTCClient = function(myAddress, video_canvas, audio_ele, disconnected) {
  let self = this;
  let localVideo = audio_ele;//let localVideo = document.getElementById('local_video');
  let remoteVideo = video_canvas;//document.getElementById('remote_video');
  let localStream = null;
  let peerConnection = null;
  let textToReceiveSdp = null;
  //let textForSendSdp = document.getElementById('text_for_send_sdp');
  //let textToReceiveSdp = document.getElementById('text_for_receive_sdp');
  let ws = null;
  let disconnectedCallback = disconnected;

  //add
  let useMedia = {
    video: false,
    audio: true
  }
  
  // --- prefix -----
  navigator.getUserMedia  = navigator.getUserMedia    || navigator.webkitGetUserMedia ||
                            navigator.mozGetUserMedia || navigator.msGetUserMedia;
  RTCPeerConnection = window.RTCPeerConnection || window.webkitRTCPeerConnection || window.mozRTCPeerConnection;
  RTCSessionDescription = window.RTCSessionDescription || window.webkitRTCSessionDescription || window.mozRTCSessionDescription;

  // -------- websocket ----
  // please use node.js app
  //  
  // or you can use chrome app (only work with Chrome)
  //  https://chrome.google.com/webstore/detail/simple-message-server/bihajhgkmpfnmbmdnobjcdhagncbkmmp
  //
  self.connectWebSocket = function(mode) {
    var wsUrl = 'ws://' + myAddress;
    ws = new WebSocket(wsUrl);

    ws.onopen = function(evt) {
      console.log('ws open()');
      ws.send(mode);
    };
    ws.onclose = function(evt) {
      disconnectedCallback();
    };
    ws.onerror = function(err) {
      console.error('ws onerror() ERR:', err);
    };
    ws.onmessage = function(evt) {
      console.log('ws onmessage() data:', evt.data);
      let message = JSON.parse(evt.data);
      if (message.type === 'offer') {
        // -- got offer ---
        console.log('Received offer ...');
        textToReceiveSdp = message.sdp;
        let offer = new RTCSessionDescription(message);
        setOffer(offer);
      }
      else if (message.type === 'answer') {
        // --- got answer ---
        console.log('Received answer ...');
        textToReceiveSdp = message.sdp;
        let answer = new RTCSessionDescription(message);
        setAnswer(answer);
      }
      else if (message.type === 'candidate') {
        // --- got ICE candidate ---
        console.log('Received ICE candidate ...');
        let candidate = new RTCIceCandidate(message.ice);
        console.log(candidate);
        addIceCandidate(candidate);
      }
    };
  }

  // ---------------------- media handling ----------------------- 
  // start local video
  self.startVideo = function() {
    //getDeviceStream({video: false, audio: true})
    getDeviceStream(useMedia)
    .then(function (stream) { // success
      localStream = stream;
      playVideo(localVideo, stream, true);
      
    }).catch(function (error) { // error
      console.error('getUserMedia error:', error);
      return;
    });
  }

  // stop local video
  self.stopVideo = function() {
    pauseVideo(localVideo);
    stopLocalStream(localStream);
  }

  let stopLocalStream = function (stream) {
    let tracks = stream.getTracks();
    if (! tracks) {
      console.warn('NO tracks');
      return;
    }
    
    for (let track of tracks) {
      track.stop();
    }
  }
  
  let getDeviceStream = function (option) {
    if ('getUserMedia' in navigator.mediaDevices) {
      console.log('navigator.mediaDevices.getUserMadia');
      return navigator.mediaDevices.getUserMedia(option);
    }
    else {
      console.log('wrap navigator.getUserMadia with Promise');
      return new Promise(function(resolve, reject){    
        navigator.getUserMedia(option,
          resolve,
          reject
        );
      });      
    }
  }

  let playVideo = function (element, stream, isMute) {
    if ('srcObject' in element) {
      element.srcObject = stream;
    }
    else {
      element.src = window.URL.createObjectURL(stream);
    }
    element.play();
    if (isMute) {
      element.volume = 0;
    } else {
      element.volume = 1;
    }
    
  }

  let pauseVideo = function (element) {
    element.pause();
    if ('srcObject' in element) {
      element.srcObject = null;
    }
    else {
      if (element.src && (element.src !== '') ) {
        window.URL.revokeObjectURL(element.src);
      }
      element.src = '';
    }
  }

  // ----- hand signaling ----
  let onSdpText = function () {
    let text = textToReceiveSdp;
    if (peerConnection) {
      console.log('Received answer text...');
      let answer = new RTCSessionDescription({
        type : 'answer',
        sdp : text,
      });
      setAnswer(answer);
    }
    else {
      console.log('Received offer text...');
      let offer = new RTCSessionDescription({
        type : 'offer',
        sdp : text,
      });
      setOffer(offer);
    }
    textToReceiveSdp ='';
  }
 
  let sendSdp = function (sessionDescription) {
    console.log('---sending sdp ---');

    //textForSendSdp.value = sessionDescription.sdp;
    /*---
    textForSendSdp.focus();
    textForSendSdp.select();
    ----*/

    let message = JSON.stringify(sessionDescription);
    console.log('sending SDP=' + message);
    ws.send(message);
  }

  let sendIceCandidate = function (candidate) {
    console.log('---sending ICE candidate ---');
    let obj = { type: 'candidate', ice: candidate };
    let message = JSON.stringify(obj);
    console.log('sending candidate=' + message);
    ws.send(message);
  }

  // ---------------------- connection handling -----------------------
  let prepareNewConnection = function () {
    let pc_config = {"iceServers":[]};
    let peer = new RTCPeerConnection(pc_config);

    // --- on get remote stream ---
    if ('ontrack' in peer) {
      peer.ontrack = function(event) {
        console.log('-- peer.ontrack()');
        let stream = event.streams[0];
        playVideo(remoteVideo, stream, false);
      };
    }
    else {
      peer.onaddstream = function(event) {
        console.log('-- peer.onaddstream()');
        let stream = event.stream;
        playVideo(remoteVideo, stream, false);
      };
    }

    // --- on get local ICE candidate
    peer.onicecandidate = function (evt) {
      if (evt.candidate) {
        console.log(evt.candidate);

        // Trickle ICE の場合は、ICE candidateを相手に送る
        sendIceCandidate(evt.candidate);

        // Vanilla ICE の場合には、何もしない
      } else {
        console.log('empty ice event');

        // Trickle ICE の場合は、何もしない
        
        // Vanilla ICE の場合には、ICE candidateを含んだSDPを相手に送る
        //sendSdp(peer.localDescription);
      }
    };

    // --- when need to exchange SDP ---
    peer.onnegotiationneeded = function(evt) {
      console.log('-- onnegotiationneeded() ---');
    };

    // --- other events ----
    peer.onicecandidateerror = function (evt) {
      console.error('ICE candidate ERROR:', evt);
    };

    peer.onsignalingstatechange = function() {
      console.log('== signaling status=' + peer.signalingState);
    };

    peer.oniceconnectionstatechange = function() {
      console.log('== ice connection status=' + peer.iceConnectionState);
      if (peer.iceConnectionState === 'disconnected') {
        console.log('-- disconnected --');
        hangUp();
      }
    };

    peer.onicegatheringstatechange = function() {
      console.log('==***== ice gathering state=' + peer.iceGatheringState);
    };
    
    peer.onconnectionstatechange = function() {
      console.log('==***== connection state=' + peer.connectionState);
    };

    peer.onremovestream = function(event) {
      console.log('-- peer.onremovestream()');
      pauseVideo(remoteVideo);
    };
    
    
    // -- add local stream --
    if (localStream) {
      console.log('Adding local stream...');
      peer.addStream(localStream);
    }
    else {
      console.warn('no local stream, but continue.');
    }

    return peer;
  }

  let makeOffer = function () {
    peerConnection = prepareNewConnection();
    peerConnection.createOffer()
    .then(function (sessionDescription) {
      console.log('createOffer() succsess in promise');
      return peerConnection.setLocalDescription(sessionDescription);
    }).then(function() {
      console.log('setLocalDescription() succsess in promise');

      // -- Trickle ICE の場合は、初期SDPを相手に送る -- 
      sendSdp(peerConnection.localDescription);

      // -- Vanilla ICE の場合には、まだSDPは送らない --
    }).catch(function(err) {
      console.error(err);
    });
  }

  let setOffer = function (sessionDescription) {
    if (peerConnection) {
      console.error('peerConnection alreay exist!');
    }
    peerConnection = prepareNewConnection();
    peerConnection.setRemoteDescription(sessionDescription)
    .then(function() {
      console.log('setRemoteDescription(offer) succsess in promise');
      makeAnswer();
    }).catch(function(err) {
      console.error('setRemoteDescription(offer) ERROR: ', err);
    });
  }
  
  let makeAnswer = function () {
    console.log('sending Answer. Creating remote session description...' );
    if (! peerConnection) {
      console.error('peerConnection NOT exist!');
      return;
    }
    
    peerConnection.createAnswer()
    .then(function (sessionDescription) {
      console.log('createAnswer() succsess in promise');
      //bitrate
      sessionDescription.sdp = setVideoAndAudioBitrates(sessionDescription.sdp, 2500, 100);
      //yoshi
      //sessionDescription.sdp = setH264(sessionDescription.sdp);

      return peerConnection.setLocalDescription(sessionDescription);
    }).then(function() {
      console.log('setLocalDescription() succsess in promise');

      // -- Trickle ICE の場合は、初期SDPを相手に送る -- 
      sendSdp(peerConnection.localDescription);

      // -- Vanilla ICE の場合には、まだSDPは送らない --
    }).catch(function(err) {
      console.error(err);
    });
  }

  let setAnswer = function (sessionDescription) {
    if (! peerConnection) {
      console.error('peerConnection NOT exist!');
      return;
    }

    peerConnection.setRemoteDescription(sessionDescription)
    .then(function() {
      console.log('setRemoteDescription(answer) succsess in promise');
    }).catch(function(err) {
      console.error('setRemoteDescription(answer) ERROR: ', err);
    });
  }

  // --- tricke ICE ---
  let addIceCandidate = function (candidate) {
    if (peerConnection) {
      peerConnection.addIceCandidate(candidate);
    }
    else {
      console.error('PeerConnection not exist!');
      return;
    }
  }
  
  // start PeerConnection
  let connect = function () {
    if (! peerConnection) {
      console.log('make Offer');
      makeOffer();
    }
    else {
      console.warn('peer already exist.');
    }
  }

  // close PeerConnection
  let hangUp = function () {
    if (peerConnection) {
      console.log('Hang up.');
      peerConnection.close();
      peerConnection = null;
      pauseVideo(remoteVideo);
    }
    else {
      console.warn('peer NOT exist.');
    }
  }

  function setVideoAndAudioBitrates(sdp, v_rate, a_rate) {
    return setMediaBitrate(setMediaBitrate(sdp, "video", v_rate), "audio", a_rate);
  }

  // https://webrtchacks.com/limit-webrtc-bandwidth-sdp/
  function setMediaBitrate(sdp, media, bitrate) {
    var lines = sdp.split("\n");
    var line = -1;
    for (var i = 0; i < lines.length; i++) {
      if (lines[i].indexOf("m="+media) === 0) {
        line = i;
        break;
      }
    }
    if (line === -1) {
      console.debug("Could not find the m line for", media);
      return sdp;
    }
    console.debug("Found the m line for", media, "at line", line);

    // Pass the m line
    line++;

    // Skip i and c lines
    while(lines[line].indexOf("i=") === 0 || lines[line].indexOf("c=") === 0) {
      line++;
    }

    // If we're on a b line, replace it
    if (lines[line].indexOf("b") === 0) {
      console.debug("Replaced b line at line", line);
      lines[line] = "b=AS:"+bitrate;
      return lines.join("\n");
    }

    // Add a new b line
    console.debug("Adding new b line before line", line);
    var newLines = lines.slice(0, line)
    newLines.push("b=AS:"+bitrate)
    newLines = newLines.concat(lines.slice(line, lines.length))
    return newLines.join("\n")
  }
  
  //圧縮方式変更
  function setH264(sdp) {
    var ret = null;
    var lines = sdp.split("\n");
    var strVp8 = "m=video 9 UDP/TLS/RTP/SAVPF 96 97 98 99 100 101 102 123 108 109 124";
    var strH264 = "m=video 9 UDP/TLS/RTP/SAVPF 98 99 100 101 102 96 97 123 108 109 124";

    sdp = sdp.replace(strVp8, strH264);
   
    console.log("SDP!!:::" + sdp);

    return sdp;
  }
}
  